#!/usr/bin/python3

import pickle, sys

def main():
    dict = {}

    with open('tweets.txt', 'r') as file:
        for line in file:
            line = line.split('\t')
            sentence = line[3].split()
            count = 0
            for word in sentence:
                dict1 = {}
                indices = [i for i, x in enumerate(sentence) if x == word]
                dict1[line[0]] = indices
                if word not in dict.keys():
                    dict[word] = dict1
                else:
                    dict[word].update(dict1)

    with open('dict.pickle','wb') as f:
        pickle.dump(dict,f)

if __name__ == '__main__':
    main()
