#!/usr/bin/python3

import sys

def bigram(l1,l2):
    l1 = iter(l1)
    l2 = iter(l2)

    p1 = next(l1)
    p2 = next(l2)
    while True:
        try:
            if p1 == (p2 + 1) or p2 == (p1 + 1):
                return True
            if p1 < p2:
                p1 = next(l1)
            if p2 < p1:
                p2 = next(l2)
            if p1 == p2:
                p1 = next(l1)
                p2 = next(l2)
        except StopIteration:
            return False
def main():
    l1 = [1, 3, 6]
    l2 = [1, 5, 8]
    print(bigram(l1, l2))

if __name__ == '__main__':
    main()
