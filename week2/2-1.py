#!/usr/bin/python3

import sys
from collections import defaultdict

def main():

    for line in sys.stdin:
        dict = defaultdict(int)
        line = line.split('\t')
        sentence = line[3]
        sentence = sentence.split()
        count = 0
        for word in sentence:
            if word not in dict.keys():
                dict[word] = [count]
                count = count + 1
            else:
                dict[word].append(count)
                count = count + 1
        for word in dict.keys():
            print(word, dict[word])

if __name__ == '__main__':
    main()
