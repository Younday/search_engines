#!/usr/bin/python3

import pickle, sys

def intersect_set(l1, l2):
    aset = set(l1)
    bset = set(l2)
    intersect_set = aset & bset
    return list(intersect_set)

def bigram(l1,l2):
    l1 = iter(l1)
    l2 = iter(l2)
    p1 = next(l1)
    p2 = next(l2)
    while True:
        try:
            if int(p2) == int(p1 + 1):
                return True
            if p1 < p2:
                p1 = next(l1)
            if p2 < p1:
                p2 = next(l2)
            if p1 == p2:
                p1 = next(l1)
                p2 = next(l2)
        except StopIteration:
            return False

def main():
    dict = pickle.load(open('dict.pickle','rb'))
    tweets = pickle.load(open('tweet.pickle','rb'))
    for line in sys.stdin:
        line = line.split()
        q1 = line[0]
        q2 = line[1]
        try:
            result1 = dict[q1]
            result2 = dict[q2]
            intersection = intersect_set(result1.keys(), result2.keys())
            for key in intersection:
                p_list1 = result1[key]
                p_list2 = result2[key]
                if bigram(p_list1, p_list2):
                    print("Following tweet contains the search term: '{}'  \nTweet: {}\n".format((q1 + " " + q2), tweets[key][1]))
        except:
            print("No tweet found for search phrase: {}".format((q1 + ' ' + q2)))

if __name__ == '__main__':
    main()
