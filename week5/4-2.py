#!/usr/bin/python3

import sys, pickle, math
from collections import defaultdict

def main():
    dict = pickle.load(open('dict.pickle','rb'))
    post_dict = pickle.load(open('pldict.pickle', 'rb'))
    total_docs = len(dict.keys())

    for line in sys.stdin:
        line = line.rstrip().split()
        if len(line) == 2:
            tweet_list = []
            ids_1 = post_dict[line[0]]
            ids_2 = post_dict[line[1]]
            ids_1_len = len(ids_1)
            ids_2_len = len(ids_2)
            try:
                ids = set(ids_1 + ids_2)
            except:
                if len(ids_1) == 0:
                    ids = ids_2
                else:
                    ids = ids_1
            if len(ids) > 0:
                for id in ids:
                    tweet = dict[id][2]
                    tweet1 = tweet.split()
                    tweet_length = len(tweet1)
                    word1_amount = tweet1.count(line[0])
                    word2_amount = tweet1.count(line[1])
                    try:
                        idf1 = math.log(total_docs/ids_1_len)
                    except:
                        idf1 = 0
                    try:
                        idf2 = math.log(total_docs/ids_2_len)
                    except:
                        idf2 = 0
                    tfidf = (word1_amount * idf1) + (word2_amount * idf2)
                    tweet_list.append((tfidf, ' '.join(line), id, tweet))
                tweet_list = sorted(tweet_list)
                for item in tweet_list:
                    print("Searched words: {}\nIdentifier: {}\nTweet: {}TF-IDF: {}\n".format(item[1],item[2],item[3], item[0]))
            else:
                print("No tweet found for words: {}".format(' '.join(line)))
        else:
            print("Please enter the correct amount of words")

if __name__ == '__main__':
    main()
