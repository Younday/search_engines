#!/usr/bin/python3

import sys

def LS1(a, b):
    vowels ="aeiou"
    l1 = len(a)+1
    l2 = len(b)+1
    matrix = [[0 for x in range(l2)] for x in range(l1)]
    for i in range(1, l1):
        if a[i-1] in vowels:
            worth = i * 0.5
        else:
            worth = i * 1
        matrix[i][0] = worth

    for i in range(1, l2):
        if b[i-1] in vowels:
            worth = i * 0.5
        else:
            worth = i * 1
        matrix[0][i] = worth

    for col in range(1, l2):
        for row in range(1, l1):
            if a[row-1] == b[col-1]:
                cost = 0
            elif a[row-1] in vowels and b[col-1] in vowels and (a[row-1] != b[col-1]):
                cost = 0.5
            elif a[row-1] != b[col-1]:
                cost = 1

            if a[row-1] in vowels and b[col-1] not in vowels:
                matrix[row][col] = min(float(matrix[row-1][col] + 0.5),
                                    float(matrix[row][col-1] + 1),
                                    float(matrix[row-1][col-1] + cost))
            elif a[row-1] not in vowels and b[col-1] in vowels:
                matrix[row][col] = min(float(matrix[row-1][col] + 1),
                                    float(matrix[row][col-1] + 0.5),
                                    float(matrix[row-1][col-1] + cost))
            else:
                matrix[row][col] = min(float(matrix[row-1][col] + 1),
                                    float(matrix[row][col-1] + 1),
                                    float(matrix[row-1][col-1] + cost))
    return matrix[row][col]


def main():
    for line in sys.stdin:
        line = line.strip()
        line = line.split('\t')
        print("{}\t{}\t{}".format(line[0], line[1], LS1(line[0], line[1])))

if __name__ == '__main__':
    main()
