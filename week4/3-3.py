#!/usr/bin/python3

import sys, pickle, time
from collections import defaultdict

def jaccard_similarity(query, document):
    intersection = set(query).intersection(set(document))
    union = set(query).union(set(document))
    return len(intersection)/len(union)

def levenshtein(s, t):
    l1 = len(s)+1
    l2 = len(t)+1
    dist = [[0 for x in range(l2)] for x in range(l1)]
    for i in range(1, l1):
        dist[i][0] = i
    for i in range(1, l2):
        dist[0][i] = i

    for col in range(1, l2):
        for row in range(1, l1):
            if s[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 1
            dist[row][col] = min(dist[row-1][col] + 1, dist[row][col-1] + 1, dist[row-1][col-1] + cost)

    return dist[row][col]

def ff():
    """Function that is needed to make sure the defaultdict works
    after pickle load"""
    return defaultdict(int)

def main():
    dict = pickle.load(open('dict.pickle','rb'))
    post_dict = pickle.load(open('post_dict.pickle', 'rb'))
    words = [x for x in post_dict.keys()]

    for line in sys.stdin:
        id1 = []
        word_dict = {}
        line = line.split()
        word_list = []
        word_list1 = []
        if len(line) == 1:
            ids = post_dict[line[0]]
            if len(ids) > 1:
                for id in ids:
                    tweet = dict[id][2]
                    print("Searched word: {}\nIdentifier: {}\nTweet: {}".format(line[0],id,tweet))
            elif len(ids) == 1:
                tweet = dict[ids][2]
                print("Searched word: {}\nIdentifier: {}\nTweet: {}".format(line,ids,tweet))
            else:
                word_list = [(levenshtein(x, line[0]), x) for x in words]
                word_list = sorted(word_list)
                minimum = word_list[0][0]
                word_list1 = [(x[0],x[1]) for x in word_list if x[0] == minimum]

                id1 = [set(post_dict[x[1]]) for x in word_list1]
                words = [x[1] for x in word_list1]
                item = set.union(*id1)
                chosen_word = ""
                print("No hits for word: {}".format(line[0]))
                print("Search for the following words due to spell correction: {}".format(words))
                for id in item:
                    tweet = dict[id][2]
                    print("Identifier: {}\nTweet: {}".format(id,tweet))
        elif len(line) == 2:
            pass

if __name__ == '__main__':
    main()
