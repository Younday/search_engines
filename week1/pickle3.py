#!/usr/bin/python3

import sys, pickle
from collections import defaultdict

def ff():
    return defaultdict(int)

def main():

    dict = pickle.load(open('dict.pickle','rb'))
    post_dict = defaultdict(ff)
    for id in dict:
        words = dict[id][2]
        words = words.split()
        for word in words:
            if word not in post_dict.keys():
                post_dict[word] = [id]
            else:
                post_dict[word].append(id)

    with open('post_dict.pickle','wb') as f:
        pickle.dump(post_dict,f)

if __name__ == '__main__':
    main()
