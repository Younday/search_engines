#!/usr/bin/python3

import pickle

def main():
    dict = {}

    with open('tweets.txt', 'r') as file:
        for line in file:
            line = line.split('\t')
            dict[line[0]] = (line[1], line[2], line[3])

    print(dict)
    with open('dict.pickle','wb') as f:
        pickle.dump(dict,f)

if __name__ == '__main__':
    main()
