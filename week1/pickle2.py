#!/usr/bin/python3

import sys, pickle

def main():

    dict = pickle.load(open('dict.pickle','rb'))

    for line in sys.stdin:
        line = line.rstrip()
        print("{}: {}".format(line,dict[line][2]))

if __name__ == '__main__':
    main()
