#!/usr/bin/python3

import sys, pickle, time
from collections import defaultdict

def intersect_set(l1, l2):
    aset = set(l1)
    bset = set(l2)
    intersect_set = aset & bset
    return list(intersect_set)

def ff():
    """Function that is needed to make sure the defaultdict works
    after pickle load"""
    return defaultdict(int)

def main():

    dict = pickle.load(open('dict.pickle','rb'))
    post_dict = pickle.load(open('post_dict.pickle', 'rb'))

    for line in sys.stdin:
        line = line.split()
        if len(line) == 1:
            ids = post_dict[line[0]]
            if len(ids) > 1:
                for id in ids:
                    tweet = dict[id][2]
                    print("Searched word: {}\nIdentifier: {}\nTweet: {}".format(line[0],id,tweet))
            elif len(ids) == 1:
                tweet = dict[ids][2]
                print("Searched word: {}\nIdentifier: {}\nTweet: {}".format(line,ids,tweet))
            else:
                print("No tweet found for word: {}".format(line))
        elif len(line) == 2:
            ids_1 = sorted(post_dict[line[0]])
            ids_2 = sorted(post_dict[line[1]])
            start = time.time()
            intersection = intersect_set(ids_1, ids_2)
            end = time.time()
            if len(intersection) > 1:
                for id in intersection:
                    tweet = dict[id][2]
                    print("Searched words: {}\nIdentifier: {}\nTweet: {}".format(' '.join(line),id,tweet))
            elif len(intersection) == 1:
                tweet = dict[intersection][2]
                print("Searched words: {}\nIdentifier: {}\nTweet: {}".format(' '.join(line),intersection,tweet))
            else:
                print("No tweet found for words: {}".format(' '.join(line)))
            print("Search time for intersection between words '{} {}' in seconds: {}".format(line[0], line[1], end - start))

if __name__ == '__main__':
    main()
