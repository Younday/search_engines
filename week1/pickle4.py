#!/usr/bin/python3

import sys, pickle
from collections import defaultdict

def ff():
    return defaultdict(int)

def main():

    dict = pickle.load(open('dict.pickle','rb'))
    post_dict = pickle.load(open('post_dict.pickle', 'rb'))

    for line in sys.stdin:
        line = line.rstrip()
        ids = post_dict[line]

        if len(ids) > 1:
            for id in ids:
                tweet = dict[id][2]
                print("Searched word: {}\nIdentifier: {}\nTweet: {}".format(line,id,tweet))
        elif len(ids) == 1:
            tweet = dict[ids][2]
            print("Searched word: {}\nIdentifier: {}\nTweet: {}".format(line,ids,tweet))
        else:
            print("No tweet found for word: {}".format(line))

if __name__ == '__main__':
    main()
