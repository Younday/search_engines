#!/usr/bin/python3

import sys
import numpy as np


def main():
    """Main function."""
    matrix = []
    for line in sys.stdin:
        line = line.rstrip('\n')
        line = line.split('\t')
        matrix.append(line)
    matrix = np.array(matrix, dtype=float)
    x_0 = np.array(matrix[0], dtype=float)
    diff = 1
    while diff > 0.0001:
        x_1 = x_0
        x_0 = x_0.dot(matrix)
        diff_list = [abs(x_0[x] - x_1[x]) for x in range(len(x_1))]
        diff = sum(diff_list)
        rounded_list = ["%.4f" % e for e in x_0]
        print("\t".join(rounded_list))


if __name__ == '__main__':
    main()
