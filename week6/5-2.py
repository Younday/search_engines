#!/usr/bin/python3

import sys

def main():
    L1 = []
    L2 = []
    agreement = 0
    P_e = 0
    for line in sys.stdin:
        line = line.rstrip('\n')
        line = line.split('\t')
        L1.append(line[0])
        L2.append(line[1])
    for x in range(len(L1)):
        if L1[x] == L2[x]:
            agreement = agreement + 1
    P_a = agreement / len(L1)
    values = set(L1).union(set(L2))
    values = list(values)
    for x in range(len(values)):
        function = ((L1.count(values[x])) / (len(L1)) * (L2.count(values[x]) / len(L2)))
        P_e = P_e + function
    kappa = (P_a - P_e) / (1 - P_e)
    print("Kappa score: {}\nP(A): {}\nP(E): {}".format(kappa, P_a, P_e))

if __name__ == '__main__':
    main()
